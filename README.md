# MUSI-VCR3-Pano

Práctica 2 de la asignatura VCR3 del máster MUSI.  
Requiere tener el entorno de la asignatura.

## Estructura de carpetas

* common -> módulo modificado para la gestión de los descriptores
* in -> imágenes de entrada
* Notebooks -> Notebooks implementados con la solución al problema
* salida -> Salida de las imágenes (la carpeta debe existir)